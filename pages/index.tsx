import React from 'react'
// import Link from 'next/link'
import io from 'socket.io-client'

export default class Index extends React.Component {
  componentDidMount() {
    const Phaser = require('phaser');
    const socket = io(undefined, {secure: true});
    const config = {
      type: Phaser.AUTO,
      width: window.innerWidth * window.devicePixelRatio,
      height: window.innerHeight * window.devicePixelRatio,
      scene: {
        preload: preload,
        create: create,
        update: update
      }
    };

    let game = new Phaser.Game(config);
    let _this;

    function preload() {
      this.load.atlas('spritesheet', 'static/spritesheet.png', 'static/spritesheet.json');
      this.load.image('gun', 'static/gun.png');
      this.load.image('bullet', 'static/bullet.png');
      this.players = {};
      this.bullets = {};
      this.blocks = {};
      this.map = {blocks: {}, players: {}};
      this.input.maxPointers = 1;
      this.pointer = {x: 0, y: 0}
      this.cameras.main.setBounds(0, 0, 6400, 6400);
      _this = this;
    }

    function create() {
      this.keyboard = this.input.keyboard.createCursorKeys();
      this.controls = {
        up: this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.W),
        down: this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.S),
        left: this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.A),
        right: this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.D),
        one: this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.ONE),
        two: this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.TWO),
        three: this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.THREE),
        four: this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.FOUR),
        five: this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.FIVE),
        six: this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.SIX),
        seven: this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.SEVEN),
        space: this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.SPACE)
      };
      this.marker = this.add.graphics();
      this.marker.lineStyle(2, 0xffffff, 1);
      this.marker.strokeRect(0, 0, 32, 32);
      this.marker.depth = 1;

      this.map_frame = this.add.graphics().setScrollFactor(0);
      this.map_frame.fillStyle(0x000000);
      this.map_frame.fillRect(6, 6, 202, 202);
      this.map_frame.lineStyle(2, 0xffffff, 1);
      this.map_frame.strokeRect(6, 6, 202, 202);
      this.map_frame.depth = 3;
   
      this.currentColour = 0x00FF00;
   
      this.input.on("pointerdown", () => { socket.emit("click",this.currentColour); });
      socket.on('update', (data) => {
        for (let i = 0; i < data.players.length; i++) {
          if (_this.players[data.players[i].id] === undefined) {
            _this.players[data.players[i].id] = {};
            _this.players[data.players[i].id].sprite = _this.add.sprite(data.players[i].x, data.players[i].y, 'spritesheet', 'skins/' + data.players[i].skin as string + '.png');
            _this.players[data.players[i].id].sprite.setDisplaySize(64, 64);
            _this.players[data.players[i].id].sprite.flipX = true;
            _this.players[data.players[i].id].sprite.depth = 1;
            _this.players[data.players[i].id].gun = _this.add.sprite(data.players[i].x - 32, data.players[i].y, 'gun');
            _this.players[data.players[i].id].gun.setDisplaySize(32, 32);
            _this.players[data.players[i].id].gun.setOrigin(0.3, 0.3);
            _this.players[data.players[i].id].gun.depth = 2;
            if (data.players[i].id === socket.id) {
              _this.cameras.main.startFollow(_this.players[data.players[i].id].sprite);
            }
            _this.map.players[data.players[i].id] = _this.add.graphics().setScrollFactor(0);
            _this.map.players[data.players[i].id].fillStyle(data.players[i].id === socket.id ? 0xD3FFD3 : 0xFFD3D3);
            _this.map.players[data.players[i].id].fillRect(0, 0, 1, 1);
            _this.map.players[data.players[i].id].depth = 4;

          } else {
            _this.players[data.players[i].id].sprite.x = data.players[i].x;
            _this.players[data.players[i].id].sprite.y = data.players[i].y;
            let angle = Phaser.Math.Angle.Between(data.players[i].x, data.players[i].y,
              data.players[i].pointer.x, data.players[i].pointer.y);
            _this.players[data.players[i].id].sprite.angle = (360 / (2 * Math.PI)) * angle - 180;
            _this.players[data.players[i].id].gun.x = 40 * Math.cos(angle) + data.players[i].x;
            _this.players[data.players[i].id].gun.y = 40 * Math.sin(angle) + data.players[i].y;
            _this.players[data.players[i].id].gun.angle = (360 / (2 * Math.PI)) * angle - 180 - 5;
            _this.map.players[data.players[i].id].x = 8 + Math.floor(data.players[i].x / 32);
            _this.map.players[data.players[i].id].y = 8 + Math.floor(data.players[i].y / 32);
          }
        }
        for (let i = 0; i < data.bullets.length; i++) {
          if (_this.bullets[data.bullets[i].id] === undefined) {
            _this.bullets[data.bullets[i].id] = _this.add.sprite(data.bullets[i].x, data.bullets[i].y, 'bullet');
            _this.bullets[data.bullets[i].id].setDisplaySize(16, 16);
          } else {
            _this.bullets[data.bullets[i].id].x = data.bullets[i].x;
            _this.bullets[data.bullets[i].id].y = data.bullets[i].y;
          }
        }
        for (let i = 0; i < data.blocks.length; i++) {
          let coords = Math.floor((data.blocks[i].x - 16) / 32) + "," + Math.floor((data.blocks[i].y - 16) / 32)
          if (_this.blocks[coords] === undefined) {
            _this.blocks[coords] = _this.add.graphics();
            _this.blocks[coords].fillStyle(data.blocks[i].color===undefined ? 0xFFFF00 : data.blocks[i].color);
            _this.blocks[coords].fillRect(data.blocks[i].x - 16, data.blocks[i].y - 16, 32, 32);
            _this.blocks[coords].depth = 1;
            _this.map.blocks[coords] = _this.add.graphics().setScrollFactor(0);
            _this.map.blocks[coords].fillStyle(data.blocks[i].color===undefined ? 0xFFFF00 : data.blocks[i].color);
            _this.map.blocks[coords].fillRect(8 + Math.floor((data.blocks[i].x - 16) / 32),
                                      8 + Math.floor((data.blocks[i].y - 16) / 32), 1, 1);
            _this.map.blocks[coords].depth = 4;
          }
        }
      });
      socket.on('remove_player', (data) => {
        if (_this !== undefined && _this.players[data] !== undefined) {
          _this.players[data].gun.destroy();
          _this.players[data].sprite.destroy();
          delete _this.players[data];
          _this.map.players[data].destroy();
          delete _this.map.players[data];
        }
      });
      socket.on('remove_block', function(data) {
        if(_this !== undefined && _this.blocks[data]) {
          _this.blocks[data].destroy();
          delete _this.blocks[data];
          _this.map.blocks[data].destroy();
          delete _this.map.blocks[data];
        }
      });
      socket.on('remove_bullet', (data) => {
        if(_this !== undefined && _this.bullets[data]) {
          _this.bullets[data].destroy();
          delete _this.bullets[data];
        }
      });
    }

    function update() {
      this.pointer = this.cameras.main.getWorldPoint(this.input.activePointer.x, this.input.activePointer.y);
      this.marker.x = Math.floor(this.pointer.x / 32) * 32;
      this.marker.y = Math.floor(this.pointer.y / 32) * 32;
      socket.emit('pointer', {x: this.pointer.x, y: this.pointer.y});
      if(this.keyboard.left.isDown || this.controls.left.isDown){
        socket.emit('move_player', {axis: 'x', force: -1});
      }
      if(this.keyboard.right.isDown || this.controls.right.isDown){
        socket.emit('move_player', {axis: 'x', force: 1});
      }
      if(this.keyboard.up.isDown || this.controls.up.isDown){
        socket.emit('move_player', {axis: 'y', force: -1});
      }
      if(this.keyboard.down.isDown || this.controls.down.isDown){
        socket.emit('move_player', {axis: 'y', force: 1});
      }
      if(this.controls.one.isDown){
        this.currentColour = 0xFF00FF;
      }
      if(this.controls.two.isDown){
        this.currentColour = 0xFFFF00;
      }
      if(this.controls.three.isDown){
        this.currentColour = 0x00FFFF;
      }
      if(this.controls.four.isDown){
        this.currentColour = 0xFF0000;
      }
      if(this.controls.five.isDown){
        this.currentColour = 0x00FF00;
      }
      if(this.controls.six.isDown){
        this.currentColour = 0x0000FF;
      }
      if(this.controls.seven.isDown){
        this.currentColour = 0xFFFFFF;
      }
      if(this.controls.space.isDown){
        socket.emit('shoot');
      }
    }
  }
  render() {
    return (
      <div className="phaserContainer" id="phaser-container">
        <style>{`
        body {
          margin: 0;
          padding: 0;
        }
        `}</style>
      </div>
    )
  }
}

// export default () => (
//   <ul>
//     <li><Link href='/a' as='/a'><a>a</a></Link></li>
//     <li><Link href='/b' as='/b'><a>b</a></Link></li>
//   </ul>
// )
