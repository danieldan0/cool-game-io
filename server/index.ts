import * as express from 'express'
import * as next from 'next'
import * as socket_io from 'socket.io'
import * as p2 from 'p2'
import * as uuid from 'uuid/v1'

const port = parseInt(process.env.PORT, 10) || 3000
const dev = process.env.NODE_ENV !== 'production'

const app = express()
const server = app.listen(port, (err) => {
  if (err) throw err
  console.log(`> Starting on http://localhost:${port}`)
})
const io = socket_io.listen(server)

const next_app = next({ dev })
const handle = next_app.getRequestHandler()

let last_time = (new Date).getTime()
const time_step = 1 / 60
let world = new p2.World({
  gravity: [0,0]
})

let wall0 = new p2.Body({
  mass: 0,
  position: [0,0],
  angle: 0
});
let wall0_shape = new p2.Plane();
wall0.addShape(wall0_shape);
world.addBody(wall0);
let wall1 = new p2.Body({
  mass: 0,
  position: [0,6400],
  angle: Math.PI
});
let wall1_shape = new p2.Plane();
wall1.addShape(wall1_shape);
world.addBody(wall1);
let wall2 = new p2.Body({
  mass: 0,
  position: [0,0],
  angle: Math.PI * 1.5
});
let wall2_shape = new p2.Plane();
wall2.addShape(wall2_shape);
world.addBody(wall2);
let wall3 = new p2.Body({
  mass: 0,
  position: [6400,0],
  angle: Math.PI / 2
});
let wall3_shape = new p2.Plane();
wall3.addShape(wall3_shape);
world.addBody(wall3);

let players = {}
let remove_queue = []
let bullets = {}
let bullet_queue = []
let blocks = {};
let blocks_queue = [];

world.on('beginContact', (evt) => {
  if (evt.bodyA._type == 'block' && evt.bodyB._type == 'bullet') {
    blocks_queue.push(evt.bodyA._id);
    bullet_queue.push(evt.bodyB._id);
  } else if (evt.bodyA._type == 'bullet' && evt.bodyB._type == 'block') {
    bullet_queue.push(evt.bodyA._id);
    blocks_queue.push(evt.bodyB._id);
  }
})

io.on('connect', (socket: any) => {
  players[socket.id] = new p2.Body({
    mass: 0.1,
    position: [3200, 3200],
    damping: 0.75,
    fixedRotation: true
  })
  players[socket.id].addShape(new p2.Circle({ radius: 32 }))
  world.addBody(players[socket.id])
  players[socket.id].pointer = {x: 3200, y: 3200}
  players[socket.id].shoot_time = (new Date).getTime();
  players[socket.id]._id = socket.id;
  players[socket.id]._type = 'player';
  players[socket.id].skin = Math.floor(Math.random() * 5);
  socket.on('move_player', (data) => {
    if (players[socket.id] !== undefined) {
      if (data.axis === "x") {
        players[socket.id].force = p2.vec2.fromValues(data.force * 50, 0)
      } else {
        players[socket.id].force = p2.vec2.fromValues(0, data.force * 50)
      }
    }
  })
  socket.on('pointer', (pointer) => {
    if (players[socket.id] !== undefined) {
      players[socket.id].pointer = pointer;
    }
  })
  socket.on('click', function(data) {
    if (players[socket.id] !== undefined) {
      let coords = Math.floor(players[socket.id].pointer.x / 32) + "," + Math.floor(players[socket.id].pointer.y / 32)
      if (blocks[coords] !== undefined) {
        blocks_queue.push(coords)
      } else {
        blocks[coords] = new p2.Body({
          mass: 0,
          position: [Math.floor(players[socket.id].pointer.x / 32) * 32 + 16, Math.floor(players[socket.id].pointer.y / 32) * 32 + 16]
        });
        blocks[coords].addShape(new p2.Box({width: 32, height: 32}));
        blocks[coords].color = (data == undefined) ? 0x00FFFF : data;
        blocks[coords]._type = 'block';
        blocks[coords]._id = coords;
        world.addBody(blocks[coords]);
      }
    }
  })
  socket.on('shoot', () => {
    if ((new Date).getTime() - players[socket.id].shoot_time > 200) {
      let id = uuid();
      let angle = Math.atan2(players[socket.id].pointer.y - players[socket.id].position[1],
        players[socket.id].pointer.x - players[socket.id].position[0]);
      bullets[id] = new p2.Body({
        mass: 0.01,
        position: [50 * Math.cos(angle) + players[socket.id].position[0],
                  50 * Math.sin(angle) + players[socket.id].position[1]],
        damping: 0,
        velocity: [750 * Math.cos(angle), 750 * Math.sin(angle)]
      });
      bullets[id].addShape(new p2.Circle({radius: 8}));
      world.addBody(bullets[id]);
      bullets[id].die_time = (new Date).getTime() + 750;
      players[socket.id].shoot_time = (new Date).getTime();
      bullets[id]._id = id;
      bullets[id]._type = 'bullet';
    }
  })
  socket.on('disconnect', () => {
    remove_queue.push(socket.id);
  });
})

setInterval(() => {
  let current_time = (new Date).getTime(); 
  let dt = (current_time - last_time) / 1000;
  dt = Math.min(1 / 60, dt); 
  world.step(time_step);
  for (let i = 0; i < remove_queue.length; i++) {
    if (players[remove_queue[i]] !== undefined) {
      world.removeBody(players[remove_queue[i]]);
    }
    delete players[remove_queue[i]];
    io.sockets.emit('remove_player', remove_queue[i]);
  }
  remove_queue = [];
  for (let i = 0; i < blocks_queue.length; i++) {
    if (blocks[blocks_queue[i]] !== undefined) {
      world.removeBody(blocks[blocks_queue[i]]);
    }
    delete blocks[blocks_queue[i]];
    io.sockets.emit('remove_block', blocks_queue[i]);
  }
  blocks_queue = [];
  let _players = [];
  let keys = Object.keys(players);
  keys.forEach((key) => {
    _players.push({
      x: players[key].position[0],
      y: players[key].position[1],
      pointer: players[key].pointer,
      skin: players[key].skin,
      id: key
    })
  })
  let _bullets = [];
  keys = Object.keys(bullets);
  keys.forEach(function(key) {
    if (bullets[key] !== undefined) {
      if ((bullets[key].die_time <= (new Date()).getTime()) || bullet_queue.includes(key)) {
        world.removeBody(bullets[key]);
        delete bullets[key];
        io.sockets.emit('remove_bullet', key);
      } else {
        _bullets.push({
            x: bullets[key].position[0],
            y: bullets[key].position[1],
            id: key
        });
      }
    } else {
      delete bullets[key];
      io.sockets.emit('remove_bullet', key);
    }
  })
  bullet_queue = [];
  let _blocks = [];
  keys = Object.keys(blocks);
  keys.forEach(function (key) {
    _blocks.push({
      x: blocks[key].position[0],
      y: blocks[key].position[1],
      color: blocks[key].color
    });
  });
  io.sockets.emit('update', {players: _players, bullets: _bullets, blocks: _blocks})
}, 1000 / 60)

next_app.prepare()
  .then(() => {
    app.get('/a', (req, res) => {
      return next_app.render(req, res, '/b', req.query)
    })

    app.get('/b', (req, res) => {
      return next_app.render(req, res, '/a', req.query)
    })

    app.get('/posts/:id', (req, res) => {
      return next_app.render(req, res, '/posts', { id: req.params.id })
    })

    app.get('*', (req, res) => {
      return handle(req, res)
    })
  })
